from web3 import Web3
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
from ControllerAbi import getControllerAbi
from MVIAbi import getMviAbi

# walletAddress = "0x552f3b71A0743d728e5c621106158134c36efF5e"
# private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
# controller = w3.eth.contract(address="0x89B1671E0Bdac24b44D3654D4B5EB1BD8150bb9e", abi=getControllerAbi())
# print(controller.functions.isModule("0x4D0A81BDf20e20Ad5556B073b12244135B40B13C").call())
# transaction_0 = controller.functions.addFactory("0x09Fe63ef1e0Fd6f94dfeaA05Ad1a1FC13fD8b880").buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 3000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
# print(w3.eth.send_raw_transaction(signed_txn_0.rawTransaction))
BMVI = w3.eth.contract(address="0xa9519A18298C5541A1738FbeBB895870e48e3451", abi=getMviAbi())
print(BMVI.functions.balanceOf("0x552f3b71A0743d728e5c621106158134c36efF5e").call())