async function main () {
  // We get the contract to deploy
  const Box = await ethers.getContractFactory('Trader');
  const box = await Box.deploy(["0x552f3b71A0743d728e5c621106158134c36efF5e"]);
  await box.deployed();
  console.log('Contract deployed to:', box.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });