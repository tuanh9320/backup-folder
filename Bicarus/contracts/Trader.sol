//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.6;
pragma experimental ABIEncoderV2;

interface IERC20 {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);
}

contract Trader {
    mapping (address=>bool) private owners;

    modifier onlyOwner(address _sender) {
        require(owners[_sender] || _sender == address(this));
        _;
    }

    constructor(address[] memory _owners) {
        owners[msg.sender] = true;
        
        _setOwner(_owners, true);
    }
    
    function _setOwner(address[] memory _addresses, bool isOwner) internal {
        for (uint32 i = 0; i < _addresses.length; i++) {
            if(_addresses[i] != msg.sender){
                owners[_addresses[i]] = isOwner;
            }
        }
    }
    function setOwner(address[] memory _addresses, bool isOwner) external onlyOwner(msg.sender) {
        _setOwner(_addresses, isOwner);
    }

    receive() external payable {
    }

    function trade(
        address _profitToken,
        uint256 _expectProfitTokenAmount,
        bool _negativeProfit,
        uint256 _ethAmountToCoinbase,
        address[] memory _targets,//to
        uint256[] memory _values,
        bytes[] memory _payloads//param
    ) external onlyOwner(msg.sender) payable {
        // get current balance
        uint256 _balanceBefore = _getBalance(_profitToken);//balance trước khi trade
        
        // trade
        for (uint256 i = 0; i < _targets.length; i++) {
            (bool _success,) = _targets[i].call{value: _values[i]}(_payloads[i]);
            require(_success);
        }

        // check profit
        //uint256 _balanceAfter = _getBalance(_profitToken);//balance sau khi trade
        
        //if(_profitToken == address(0)) {
        //    require(_balanceAfter >= _balanceBefore + _ethAmountToCoinbase);
        //} else {
        //    if(_negativeProfit){
        //        require(_balanceAfter >= _balanceBefore - _expectProfitTokenAmount);
        //    } else {
        //        require(_balanceAfter >= _balanceBefore + _expectProfitTokenAmount);
        //    }
        //}
        
        //if (_ethAmountToCoinbase == 0) return;

        //block.coinbase.transfer(_ethAmountToCoinbase);
    }
    
    function _getBalance(address _token) view internal returns (uint256) {
        uint256 _balance;
        
        if(_token == address(0)){
            _balance = address(this).balance;
        } else {
            _balance = IERC20(_token).balanceOf(address(this));
        }
        
        return _balance;
    }
}