from web3 import Web3
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
from CreateSetTokenABI import getCreateSetTokenAbi

walletAddress = "0x552f3b71A0743d728e5c621106158134c36efF5e"
private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
createSetToken = w3.eth.contract(address="0x09Fe63ef1e0Fd6f94dfeaA05Ad1a1FC13fD8b880", abi=getCreateSetTokenAbi())
# print(createSetToken.functions.controller().call())

components = ["0x66B72bE3d46714D729f2392c1888E459AC12d756", "0x524249f31A9c5bD06fF8b651E21B77Ce9E2F4a36", "0xA56d3558eE8a5b828881372Bc5266a7707AA1793"]
units = [100000000000000000000, 10000000000000000000, 80000000000000000000]
modules = ["0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", "0x5A8b6338a0E9621D17e9f6050cD46325acB7113C", "0x79f5CB51D7dBfDF57B090222Ba799C634e9D3AC1", "0x0726A979163ce6008f502a0267539a67d5E2EEc3", "0x36D30e8Be60291FaBc8717126Be9d51d7ac51569"]
manager = "0x552f3b71A0743d728e5c621106158134c36efF5e"
name = "Bicarus MVI"
symbol = "BIC"

transaction_0 = createSetToken.functions.create(components, units, modules, manager, name, symbol).buildTransaction({
    'chainId':5,
    'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
    'gas': 5000000,
    'maxFeePerGas': 57562177587,
    'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
    'nonce': w3.eth.getTransactionCount(walletAddress)
})
signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
print(w3.eth.send_raw_transaction(signed_txn_0.rawTransaction))


# from SetTokenAbi import getSetTokenAbi
# BMVI = w3.eth.contract(address="0x8c9938F4dBD7b83f60A5d8fADDf9cCF4786A5806", abi=getSetTokenAbi())
# print(BMVI.functions.manager().call())