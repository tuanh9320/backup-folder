from web3 import Web3
from PairAbi import getPairAbi
from abiMVI import getMVIAbi
from Erc20Abi import getErc20Abi
from sushiABI import getSushiABI
from UniswapPairAddress import getUniswapPair
from FactoryAbi import getFactoryAbi
from swapAbi import getInterface
import threading
import time
from decimal import *
time1 = time.time()
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545/"))
getcontext().prec = 28
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
childToken = w3.eth.contract(address="0xd8EF3cACe8b4907117a45B0b125c68560532F94D", abi=getMVIAbi())
data = childToken.functions.getRequiredComponentUnitsForIssue(Web3.toChecksumAddress('0x72e364f2abdc788b7e918bc238b21f109cd634d7'), 1000000000000000000).call()
tokenAddress = data[0]
quantityOfChildToken = len(tokenAddress)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get decimal
decimals = []
for i in tokenAddress:
	temp = w3.eth.contract(address=Web3.toChecksumAddress(i), abi=getErc20Abi())
	decimals.append(temp.functions.decimals().call())
decimals.append(18)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
mintData = []
count = 0
for i in data[1]:
	mintData.append(i/10**decimals[count])
	count += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get all pair address
sushiPairAddress = []
uniPairAddress = []
sushiFactory = w3.eth.contract(address="0xC0AEe478e3658e2610c5F7A4A2E1777cE9e4f2Ac", abi=getFactoryAbi())
uniFactory = w3.eth.contract(address="0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f", abi=getFactoryAbi())
for token in tokenAddress:
	uniPairAddress.append(uniFactory.functions.getPair(token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2").call())
	sushiPairAddress.append(sushiFactory.functions.getPair(token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2").call())
uniPairAddress.append(uniFactory.functions.getPair("0x72e364F2ABdC788b7E918bc238B21f109Cd634D7", "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2").call())#MVI-WETH
sushiPairAddress.append(sushiFactory.functions.getPair("0x72e364F2ABdC788b7E918bc238B21f109Cd634D7", "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2").call())#MVI-WETH
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get reserves
wethReserves = 0
mviReserves = 0
pairNotExistInUni = []
pairNotExistInSushi = []
reservesUNI = []
count1 = 0
for i in uniPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesUNI.append(0)
		reservesUNI.append(0)
		pairNotExistInUni.append(count1)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2':
			reservesUNI.append(reserves[0]/1000000000000000000)
			reservesUNI.append(reserves[1]/10**decimals[count1])
		else:
			if token0 == "0x72e364F2ABdC788b7E918bc238B21f109Cd634D7":
				wethReserves = reserves[1]
				mviReserves = reserves[0]
			reservesUNI.append(reserves[1]/1000000000000000000)
			reservesUNI.append(reserves[0]/10**decimals[count1])
	count1 += 1
count2 = 0
reservesSUSHI = []
for i in sushiPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesSUSHI.append(0)
		reservesSUSHI.append(0)
		pairNotExistInSushi.append(count2)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2':
			reservesSUSHI.append(reserves[0]/1000000000000000000)
			reservesSUSHI.append(reserves[1]/10**decimals[count2])
		else:
			reservesSUSHI.append(reserves[1]/1000000000000000000)
			reservesSUSHI.append(reserves[0]/10**decimals[count2])
	count2 += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#The function that help us decide which will should be used to swap token
def whichBeUsedToSwap(x, index):
	x = x*mintData[index]
	temp = reservesUNI[2*index]*reservesSUSHI[2*index+1] - reservesUNI[2*index+1]*reservesSUSHI[2*index] + 0.997*x*(reservesUNI[2*index] - reservesSUSHI[2*index])
	if index in pairNotExistInSushi:
		return 1
	elif index in pairNotExistInUni:
		return 2
	elif temp > 0:
		return 1
	else:
		return 2
def calculateBenefit(y):
	benefit = -(reservesUNI[2*quantityOfChildToken]*y*1000)/(997*reservesUNI[2*quantityOfChildToken+1] - 997*y)
	for i in range(quantityOfChildToken):
		if whichBeUsedToSwap(y, i)==2:
			benefit = benefit + (997*mintData[i]*reservesSUSHI[2*i]*y)/(1000*reservesSUSHI[2*i+1]+997*mintData[i]*y)
		else:
			benefit = benefit + (997*mintData[i]*reservesUNI[2*i]*y)/(1000*reservesUNI[2*i+1]+997*mintData[i]*y)
	return benefit
def getWhichUsedToSwap(y):
	whichUsedToSwap = []
	benefit = -(reservesUNI[2*quantityOfChildToken]*y*1000)/(997*reservesUNI[2*quantityOfChildToken+1] - 997*y)
	for i in range(quantityOfChildToken):
		if whichBeUsedToSwap(y, i)==2:
			whichUsedToSwap.append(2)
			benefit = benefit + (997*mintData[i]*reservesSUSHI[2*i]*y)/(1000*reservesSUSHI[2*i+1]+997*mintData[i]*y)
		else:
			whichUsedToSwap.append(1)
			benefit = benefit + (997*mintData[i]*reservesUNI[2*i]*y)/(1000*reservesUNI[2*i+1]+997*mintData[i]*y)
	return whichUsedToSwap
def calculateMaximumBenefit(left, right):
	timestamp = time.time()
	while right-left > 0.00000001:
		if time.time()-timestamp > 0.01:
			print("out")
			return 0
		x1 = left + (right-left)/3.0
		x2 = right - (right-left)/3.0
		if calculateBenefit(x1) > calculateBenefit(x2):
			right=x2
		else: 
			left=x1
	return right
def getCalculationResult():
	right = reservesUNI[quantityOfChildToken*2 + 1]
	print(right)
	y = calculateMaximumBenefit(0, right)
	result = Decimal(1000*reservesUNI[quantityOfChildToken*2]*y/(997*(reservesUNI[quantityOfChildToken*2 + 1]-y)))
	uniswap = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())
	temp = int(result*10**18)
	amoutOfProfitExpected_ = calculateBenefit(y)
	amoutOfProfitExpected = int(amoutOfProfitExpected_*10**18)
	x = Decimal(temp)/Decimal(10**18)
	rIn = Decimal(wethReserves)/Decimal(10**18)
	rOut = Decimal(mviReserves)/Decimal(10**18)
	amoutMviOut =int((997*x*rOut)*10**18/(1000*rIn+997*x))
	whichUsedToSwap = getWhichUsedToSwap(y)
	return [temp, amoutMviOut, amoutOfProfitExpected_, data[1], whichUsedToSwap]
print(getCalculationResult())