from web3 import Web3
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
from MVIAbi import getMviAbi
from erc20Abi import getErc20Abi
import time
from BasicIssuanceModuleAbi import getBasicIssuanceModuleAbi
from StreamingFeeModule import getStreamingFeeModuleAbi
from TradeFeeModuleAbi import getTradeFeeModuleAbi
from PairAbi import getPairAbi
private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
walletAddress = Web3.toChecksumAddress('0x552f3b71A0743d728e5c621106158134c36efF5e')

BMVI = w3.eth.contract(address="0xa9519A18298C5541A1738FbeBB895870e48e3451", abi=getMviAbi())

catToken = w3.eth.contract(address="0x524249f31A9c5bD06fF8b651E21B77Ce9E2F4a36", abi=getErc20Abi())
dogToken = w3.eth.contract(address="0x66B72bE3d46714D729f2392c1888E459AC12d756", abi=getErc20Abi())
draToken = w3.eth.contract(address="0xA56d3558eE8a5b828881372Bc5266a7707AA1793", abi=getErc20Abi())

# StreamingFeeModule = w3.eth.contract(address="0x5A8b6338a0E9621D17e9f6050cD46325acB7113C", abi=getStreamingFeeModuleAbi())
# timeStamp = int(time.time())
# setting = ("0x552f3b71A0743d728e5c621106158134c36efF5e",40000000000000000,950000000000000,timeStamp)
# addressTemp = Web3.toChecksumAddress('0xa9519A18298C5541A1738FbeBB895870e48e3451')
# transaction_0 = StreamingFeeModule.functions.initialize(addressTemp, setting).buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# TradeModule = w3.eth.contract(address="0x79f5CB51D7dBfDF57B090222Ba799C634e9D3AC1", abi=getTradeFeeModuleAbi())
# transaction_0 = TradeModule.functions.initialize("0xa9519A18298C5541A1738FbeBB895870e48e3451").buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
# print(BMVI.functions.moduleStates("0x79f5CB51D7dBfDF57B090222Ba799C634e9D3AC1").call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# WrapModule = w3.eth.contract(address="0x0726A979163ce6008f502a0267539a67d5E2EEc3", abi=getTradeFeeModuleAbi())
# transaction_0 = WrapModule.functions.initialize("0xa9519A18298C5541A1738FbeBB895870e48e3451").buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
# print(BMVI.functions.moduleStates("0x0726A979163ce6008f502a0267539a67d5E2EEc3").call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# GeneralIndexModule = w3.eth.contract(address="0x36D30e8Be60291FaBc8717126Be9d51d7ac51569", abi=getTradeFeeModuleAbi())
# transaction_0 = GeneralIndexModule.functions.initialize("0xa9519A18298C5541A1738FbeBB895870e48e3451").buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(transaction_0, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
# print(BMVI.functions.moduleStates("0x36D30e8Be60291FaBc8717126Be9d51d7ac51569").call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#approve child token to contract
# txn_0 = catToken.functions.approve("0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", 1000000000000000000000000).buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_0 = w3.eth.account.sign_transaction(txn_0, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
# txn_1 = dogToken.functions.transfer("0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", 1000000000000000000000000).buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_1 = w3.eth.account.sign_transaction(txn_1, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
# txn_2 = draToken.functions.transfer("0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", 1000000000000000000000000).buildTransaction({
#     'chainId':5,
#     'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
#     'gas': 10000000,
#     'maxFeePerGas': 57562177587,
#     'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
#     'nonce': w3.eth.getTransactionCount(walletAddress)
# })
# signed_txn_2 = w3.eth.account.sign_transaction(txn_2, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
basicIssuanceModule = w3.eth.contract(address="0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", abi=getBasicIssuanceModuleAbi())
mintBMVI = basicIssuanceModule.functions.issue("0xa9519A18298C5541A1738FbeBB895870e48e3451", 1000000000000000000000, walletAddress).buildTransaction({
    'chainId':5,
    'from': "0x552f3b71A0743d728e5c621106158134c36efF5e",
    'gas': 10000000,
    'maxFeePerGas': 57562177587,
    'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
    'nonce': w3.eth.getTransactionCount(walletAddress)
})
# signed_txn_0 = w3.eth.account.sign_transaction(mintBMVI, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_0.rawTransaction)
# print(BMVI.functions.balanceOf(walletAddress).call())
# pair = w3.eth.contract(address="0x6F846BD244d39cc7DdFFdEb02dB379e144424360", abi=getPairAbi())
# print(pair.functions.token0().call())