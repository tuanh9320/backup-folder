from web3 import Web3
from interface import getInterface
import threading
import time
from Token import getERC20Abi
time1 = time.time()
# w3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
w3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
uniswap = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())#UniswapV2Router02
walletAddress = Web3.toChecksumAddress('0x416D15836c6E9c8C59fb933653d0E1829235B624')
recipient = Web3.toChecksumAddress('0x70997970c51812dc3a010c7d01b50e0d17dc79c8')


ETHToken = w3.eth.contract(address='0xc778417E063141139Fce010982780140Aa0cD5Ab', abi=getERC20Abi())
private_key = '0c848265732c7b4e5f3059e9a3ebc563129628f43dae87a81e095c25306e1768'
# Chuyen ETH sang WETH
transaction = ETHToken.functions.deposit().buildTransaction({
        'chainId':31337,
        'from': walletAddress,
        'gas': 70000,
        'value':1000000000000000000,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 73
    })
signed_txn = w3.eth.account.sign_transaction(transaction, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn.rawTransaction)
print(ETHToken.functions.balanceOf(walletAddress).call())

# Approve WETH cho Router
transaction_1 = ETHToken.functions.approve('0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', 1000000000000000000).buildTransaction({
        'chainId':31337,
        'from': walletAddress,
        'gas': 70000,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 74
    })
signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
print("Luong WETH da improve cho router:")
print(ETHToken.functions.allowance(Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266'), '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D').call())
# Swap WETH vs ENJ
timestamp = int((time.time() + 100000000000)//1)
transaction_2 = uniswap.functions.swapExactTokensForTokens(2100000000000000,0, 
    ["0xc778417E063141139Fce010982780140Aa0cD5Ab","0xaD6D458402F60fD3Bd25163575031ACDce07538D"], 
    recipient,timestamp).buildTransaction({
        'chainId':3,
        'gas': 150000,
        'from': walletAddress,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 17
    })
signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
# print(ETHToken.functions.allowance(walletAddress, '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D').call())