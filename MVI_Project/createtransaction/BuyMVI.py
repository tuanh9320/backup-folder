from web3 import Web3
from interface import getInterface
import threading
import time
from Token import getERC20Abi
time1 = time.time()
# w3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545/"))
uniswap = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())#UniswapV2Router02
walletAddress = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')
recipient = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')


WETHToken = w3.eth.contract(address='0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', abi=getERC20Abi())
private_key = '0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
transaction = WETHToken.functions.deposit().buildTransaction({
        'chainId':31337,
        'from': walletAddress,
        'gas': 70000,
        'value':9834258231187766000,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 70
    })
signed_txn = w3.eth.account.sign_transaction(transaction, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn.rawTransaction)
print(WETHToken.functions.balanceOf(walletAddress).call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
transaction_1 = WETHToken.functions.approve('0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', 9834258231187766000).buildTransaction({
        'chainId':31337,
        'from': walletAddress,
        'gas': 70000,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 71
    })
signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
print("Luong WETH da improve cho router:")
print(WETHToken.functions.allowance(Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266'), '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D').call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
timestamp = int((time.time() + 100000000000)//1)
transaction_2 = uniswap.functions.swapExactTokensForTokens(9834258231187766000,0, 
    ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2","0x72e364F2ABdC788b7E918bc238B21f109Cd634D7"], 
    recipient,timestamp).buildTransaction({
        'chainId':31337,
        'gas': 150000,
        'from': walletAddress,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 72
    })
signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)

MVIToken = w3.eth.contract(address='0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', abi=getERC20Abi())
print(MVIToken.functions.balanceOf(walletAddress).call())