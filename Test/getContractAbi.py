def getAbi():
	return([
	{
		"inputs": [
			{
				"internalType": "address[]",
				"name": "_owners",
				"type": "address[]"
			}
		],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_profitToken",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_expectProfitTokenAmount",
				"type": "uint256"
			},
			{
				"internalType": "bool",
				"name": "_negativeProfit",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "_ethAmountToCoinbase",
				"type": "uint256"
			},
			{
				"internalType": "address",
				"name": "_targets",
				"type": "address"
			},
			{
				"internalType": "bytes",
				"name": "_payloads",
				"type": "bytes"
			}
		],
		"name": "burnMVI",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address[]",
				"name": "_addresses",
				"type": "address[]"
			},
			{
				"internalType": "bool",
				"name": "isOwner",
				"type": "bool"
			}
		],
		"name": "setOwner",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_profitToken",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "_expectProfitTokenAmount",
				"type": "uint256"
			},
			{
				"internalType": "bool",
				"name": "_negativeProfit",
				"type": "bool"
			},
			{
				"internalType": "uint256",
				"name": "_ethAmountToCoinbase",
				"type": "uint256"
			},
			{
				"internalType": "address[]",
				"name": "_targets",
				"type": "address[]"
			},
			{
				"internalType": "uint256[]",
				"name": "_values",
				"type": "uint256[]"
			},
			{
				"internalType": "bytes[]",
				"name": "_payloads",
				"type": "bytes[]"
			}
		],
		"name": "trade",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"stateMutability": "payable",
		"type": "receive"
	}
])