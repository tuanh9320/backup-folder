from web3 import Web3
from getContractAbi import getAbi
from swapAbi import getInterface
from MVIAbi import getMVIAbi
from MVIIndexAbi import getMVIIndexAbi
from SetTokenAbi import getSetTokenAbi

from Token import getERC20Abi
from eth_abi import encode_abi
import time
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545/"))
walletAddress = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')
private_key = "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80"
contractAddress = '0x21dF544947ba3E8b3c32561399E88B52Dc8b2823'

MVIToken = w3.eth.contract(address='0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', abi=getMVIAbi())
uniswap = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())
sushiswap = w3.eth.contract(address="0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F", abi=getInterface())
WETHToken = w3.eth.contract(address='0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', abi=getERC20Abi())
MVIIndex = w3.eth.contract(address="0xd8EF3cACe8b4907117a45B0b125c68560532F94D", abi=getMVIIndexAbi())
contract = w3.eth.contract(address=contractAddress, abi=getAbi())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
profitToken = "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"
expectProfitTokenAmount = 188117321716445570
navigativeProfit = True
ethAmountToCoinBase = 0
target = ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2","0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", "0x72e364F2ABdC788b7E918bc238B21f109Cd634D7", "0xd8EF3cACe8b4907117a45B0b125c68560532F94D"]
value = [9834258231187766000, 0,0,0,0]
#Payload convert ETH to WETH
payload0 = WETHToken.encodeABI(fn_name="deposit", args=[])
#Payload approve
payload1 = WETHToken.encodeABI(fn_name="approve", args=["0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", 9834258231187766000])
#Payload swap WETH to MVI
timestamp = int((time.time() + 100000000000)//1)
payload2 = uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[9834258231187766000,0,["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2","0x72e364F2ABdC788b7E918bc238B21f109Cd634D7"], contractAddress, timestamp])
#Payload burn MVI token
#Payload approve for MVI contract
payload3 = MVIToken.encodeABI(fn_name="approve", args=['0xd8EF3cACe8b4907117a45B0b125c68560532F94D', 984027558590444887223])
#Payload burn MVI token
payload4 = MVIIndex.encodeABI(fn_name="redeem", args=['0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', 284027558590444887222, contractAddress])

payload = [payload0, payload1, payload2, payload3, payload4]

#The function that automatical append param to target, value, payload
SetToken = w3.eth.contract(address="0x72e364F2ABdC788b7E918bc238B21f109Cd634D7", abi=getSetTokenAbi())#UniswapV2Router02
tokenAddress = SetToken.functions.getComponents().call()#Array contain the address of 14 child token
def swapToken(token, quantity, typeSwap):
    print(quantity)
    token_ = w3.eth.contract(address=token, abi=getERC20Abi())
    if typeSwap == 1:
        tokenContract = w3.eth.contract(address=token, abi=getERC20Abi())
        target.append(token)
        target.append("0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D")
        value.append(0)
        value.append(0)
        payload.append(tokenContract.encodeABI(fn_name="approve", args=['0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', quantity]))#approve
        payload.append(uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[quantity, 0, [token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"], contractAddress, timestamp]))#swap token to WETH
    else:
        tokenContract = w3.eth.contract(address=token, abi=getERC20Abi())
        target.append(token)
        target.append("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F")
        value.append(0)
        value.append(0)
        payload.append(tokenContract.encodeABI(fn_name="approve", args=['0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F', quantity]))#approve
        payload.append(sushiswap.encodeABI(fn_name="swapExactTokensForTokens", args=[quantity, 0, [token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"], contractAddress, timestamp]))#swap token to WETH
def getAmountTokenInWallet():
    result = []
    for i in tokenAddress:
        contractOfToken = w3.eth.contract(address=i, abi=getERC20Abi())
        result.append(contractOfToken.functions.balanceOf(contractAddress).call())
    return result
amountTokenInWallet = getAmountTokenInWallet()
print(amountTokenInWallet)
for i in range(14):
    if i in [1, 6, 7, 8, 11, 12]:
        swapToken(tokenAddress[i],amountTokenInWallet[i], 2)
        break
    else:
        swapToken(tokenAddress[i],amountTokenInWallet[i], 1)
        break

nonce = w3.eth.getTransactionCount(walletAddress)
transaction_1 = contract.functions.trade(profitToken, expectProfitTokenAmount,navigativeProfit, ethAmountToCoinBase, target,value, payload).buildTransaction({
    'chainId':31337,
    'from': walletAddress,
    'value':9834258231187766000,
    'gas': 10000000,
    'maxFeePerGas': 57562177587,
    'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
    'nonce': nonce
})
signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
# print(MVIToken.functions.allowance(contractAddress, "0xd8EF3cACe8b4907117a45B0b125c68560532F94D").call())
# print(MVIToken.functions.balanceOf(contractAddress).call())
EnjToken = w3.eth.contract(address='0xF629cBd94d3791C9250152BD8dfBDF380E2a3B9c', abi=getERC20Abi())
print(WETHToken.functions.balanceOf("0x21dF544947ba3E8b3c32561399E88B52Dc8b2823").call())