from web3 import Web3
from getContractAbi import getAbi
from swapAbi import getInterface
from MVIAbi import getMVIAbi
from MVIIndexAbi import getMVIIndexAbi
from SetTokenAbi import getSetTokenAbi
import sys
sys.path.append('../Calculation')
from index import getAllParameter


from Token import getERC20Abi
from eth_abi import encode_abi
from decimal import *
import time
getcontext().prec = 100
time1 = time.time()
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
walletAddress = Web3.toChecksumAddress('0x552f3b71A0743d728e5c621106158134c36efF5e')
private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
contractAddress = '0x896426c186eE4AED7E1470EAfE059E654fADB503'
mviTokenAddress = '0xa9519A18298C5541A1738FbeBB895870e48e3451'
uniswapAddress = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
sushiswapAddress = "0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506"
wethTokenAddress = '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6'
basicIssuanceModuleAddress = "0x4D0A81BDf20e20Ad5556B073b12244135B40B13C"

MVIToken = w3.eth.contract(address=mviTokenAddress, abi=getMVIAbi())
uniswap = w3.eth.contract(address=uniswapAddress, abi=getInterface())
sushiswap = w3.eth.contract(address=sushiswapAddress, abi=getInterface())
WETHToken = w3.eth.contract(address=wethTokenAddress, abi=getERC20Abi())
MVIIndex = w3.eth.contract(address=basicIssuanceModuleAddress, abi=getMVIIndexAbi())
contract = w3.eth.contract(address=contractAddress, abi=getAbi())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Get all parametor calculated in the index.py file
calculationResult = getAllParameter()
print(calculationResult)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Transfer WETH to contract
transaction_1 = WETHToken.functions.transfer(contractAddress, calculationResult[0]).buildTransaction({
    'chainId':5,
    'from': walletAddress,
    'gas': 700000,
    'maxFeePerGas': 57562177587,
    'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
    'nonce': w3.eth.getTransactionCount(walletAddress)
})
# signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
profitToken = wethTokenAddress
navigativeProfit = False
ethAmountToCoinBase = 100000000000
if calculationResult[5] == 1:
    target = [wethTokenAddress, uniswapAddress, mviTokenAddress, basicIssuanceModuleAddress]
else:
    target = [wethTokenAddress, sushiswapAddress, mviTokenAddress, basicIssuanceModuleAddress]
value = [0, 0, 0, 0]
#Payload approve in 2 cases
if calculationResult[5] == 1:
    payload1 = WETHToken.encodeABI(fn_name="approve", args=[uniswapAddress, calculationResult[0]])
    #Payload swap WETH to MVI
    timestamp = int((time.time() + 100000000000)//1)
    payload2 = uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[calculationResult[0],0,[wethTokenAddress,mviTokenAddress], contractAddress, timestamp])
else:
    payload1 = WETHToken.encodeABI(fn_name="approve", args=[sushiswapAddress, calculationResult[0]])
    #Payload swap WETH to MVI
    timestamp = int((time.time() + 100000000000)//1)
    payload2 = sushiswap.encodeABI(fn_name="swapExactTokensForTokens", args=[calculationResult[0],0,[wethTokenAddress,mviTokenAddress], contractAddress, timestamp])
#Payload burn MVI token
#Payload approve for MVI contract
payload3 = MVIToken.encodeABI(fn_name="approve", args=[basicIssuanceModuleAddress, calculationResult[1]])
#Payload burn MVI token
payload4 = MVIIndex.encodeABI(fn_name="redeem", args=[mviTokenAddress, calculationResult[1], contractAddress])

payload = [payload1, payload2, payload3, payload4]

#The function that automatical append param to target, value, payload
SetToken = w3.eth.contract(address=mviTokenAddress, abi=getSetTokenAbi())#UniswapV2Router02
tokenAddress = SetToken.functions.getComponents().call()#Array contain the address of 14 child token
def swapToken(token, quantity, typeSwap):
    token_ = w3.eth.contract(address=token, abi=getERC20Abi())
    if typeSwap == 1:
        tokenContract = w3.eth.contract(address=token, abi=getERC20Abi())
        target.append(token)
        target.append(uniswapAddress)
        value.append(0)
        value.append(0)
        payload.append(tokenContract.encodeABI(fn_name="approve", args=[uniswapAddress, quantity]))#approve
        payload.append(uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[quantity, 0, [token, wethTokenAddress], contractAddress, timestamp]))#swap token to WETH
    else:
        tokenContract = w3.eth.contract(address=token, abi=getERC20Abi())
        target.append(token)
        target.append(sushiswapAddress)
        value.append(0)
        value.append(0)
        payload.append(tokenContract.encodeABI(fn_name="approve", args=[sushiswapAddress, quantity]))#approve
        payload.append(sushiswap.encodeABI(fn_name="swapExactTokensForTokens", args=[quantity, 0, [token, wethTokenAddress], contractAddress, timestamp]))#swap token to WETH
def getAmountTokenInWallet():
    result = []
    for i in range(len(calculationResult[3])):
        result.append(int(Decimal(Decimal(calculationResult[3][i])*calculationResult[1]/1000000000000000000)))
    return result
amountTokenInWallet = getAmountTokenInWallet()
count = 0
for i in calculationResult[4]:
    if i == 2:
        swapToken(tokenAddress[count],amountTokenInWallet[count], 2)
    else:
        swapToken(tokenAddress[count],amountTokenInWallet[count], 1)
    count += 1

nonce = w3.eth.getTransactionCount(walletAddress)
gasPrice =1000000007
expectProfitTokenAmount = 0
transaction_2 = contract.functions.trade(profitToken, expectProfitTokenAmount,navigativeProfit, ethAmountToCoinBase, target,value, payload).buildTransaction({
    'chainId':5,
    'from': walletAddress,
    'value':ethAmountToCoinBase,
    'gas': 8000000,
    'gasPrice': gasPrice,
    'nonce': nonce
})

print(w3.eth.estimateGas(transaction_2)*gasPrice)
expectProfitTokenAmount = w3.eth.estimateGas(transaction_2)*gasPrice
ethAmountToCoinBase = int(Decimal(0.8)*(Decimal(calculationResult[2])-Decimal(expectProfitTokenAmount)))
if ethAmountToCoinBase < 0:
    ethAmountToCoinBase = 0
print(ethAmountToCoinBase)
transaction_2 = contract.functions.trade(profitToken, expectProfitTokenAmount,navigativeProfit, ethAmountToCoinBase, target,value, payload).buildTransaction({
    'chainId':5,
    'from': walletAddress,
    'value':ethAmountToCoinBase,
    'gas': 8000000,
    'gasPrice': gasPrice,
    'nonce': nonce
})
signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
# print("Wallet's balance:")
# print(WETHToken.functions.balanceOf(walletAddress).call())
# print("Contract's balance:")
# print(WETHToken.functions.balanceOf(contractAddress).call())
# signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
# EnjToken = w3.eth.contract(address='0xF629cBd94d3791C9250152BD8dfBDF380E2a3B9c', abi=getERC20Abi())
# print(MVIToken.functions.balanceOf(contractAddress).call())
# print(EnjToken.functions.balanceOf(contractAddress).call())
# print(WETHToken.functions.allowance(contractAddress, uniswapAddress).call())
print(WETHToken.functions.balanceOf(contractAddress).call())
#Test to make sure that we swap all child token to WETH token
# tokenAddress_ = ['0xF629cBd94d3791C9250152BD8dfBDF380E2a3B9c', '0x0F5D2fB29fb7d3CFeE444a200298f468908cC942', '0x3845badAde8e6dFF049820680d1F14bD3903a5d0', '0x18aAA7115705e8be94bfFEBDE57Af9BFc265B998', '0x7a2Bc711E19ba6aff6cE8246C546E8c4B4944DFD', '0xBBc2AE13b23d715c30720F079fcd9B4a74093505', '0x87d73E916D7057945c9BcD8cdd94e42A6F47f776', '0xBB0E17EF65F82Ab018d8EDd776e8DD940327B28b', '0xEE06A81a695750E71a662B51066F2c74CF4478a0', '0x9355372396e3F6daF13359B7b607a3374cc638e0', '0xd084B83C305daFD76AE3E1b4E1F1fe2eCcCb3988', '0x767FE9EDC9E0dF98E07454847909b5E959D7ca0E', '0x557B933a7C2c45672B610F8954A3deB39a51A8Ca', '0xFca59Cd816aB1eaD66534D82bc21E7515cE441CF']
# for i in tokenAddress_:
#     token = w3.eth.contract(address=i, abi=getERC20Abi())
#     print(token.functions.balanceOf(contractAddress).call())