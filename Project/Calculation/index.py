from web3 import Web3
from PairAbi import getPairAbi
from abiMVI import getMVIAbi
from Erc20Abi import getErc20Abi
from sushiABI import getSushiABI
from UniswapPairAddress import getUniswapPair
from FactoryAbi import getFactoryAbi
from swapAbi import getInterface
import threading
import time
from decimal import *
import threading
time1 = time.time()
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
getcontext().prec = 28
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get address of child token
childToken = w3.eth.contract(address="0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", abi=getMVIAbi())
data = childToken.functions.getRequiredComponentUnitsForIssue(Web3.toChecksumAddress('0xa9519A18298C5541A1738FbeBB895870e48e3451'), 1000000000000000000).call()
tokenAddress = data[0]
quantityOfChildToken = len(tokenAddress)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get decimal of all token
decimals = []
for i in tokenAddress:
	temp = w3.eth.contract(address=Web3.toChecksumAddress(i), abi=getErc20Abi())
	decimals.append(temp.functions.decimals().call())
decimals.append(18)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get the number of child token that required to mint a BMVI token
mintData = []
count = 0
for i in data[1]:
	mintData.append(Decimal(i)/Decimal(10**decimals[count]))
	count += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get all pair address
sushiPairAddress = []
uniPairAddress = []
sushiFactory = w3.eth.contract(address="0xc35DADB65012eC5796536bD9864eD8773aBc74C4", abi=getFactoryAbi())
uniFactory = w3.eth.contract(address="0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f", abi=getFactoryAbi())
for token in tokenAddress:
	uniPairAddress.append(uniFactory.functions.getPair(token, "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())
	sushiPairAddress.append(sushiFactory.functions.getPair(token, "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())
uniPairAddress.append(uniFactory.functions.getPair("0xa9519A18298C5541A1738FbeBB895870e48e3451", "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())#MVI-WETH
sushiPairAddress.append(sushiFactory.functions.getPair("0xa9519A18298C5541A1738FbeBB895870e48e3451", "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())#MVI-WETH
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get reserves
wethReservesUNI = 0
mviReservesUNI = 0
wethReservesSUSHI = 0
mviReservesSUSHI = 0
pairNotExistInUni = []
pairNotExistInSushi = []
reservesUNI = []
count1 = 0
allParameter = []
for i in uniPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesUNI.append(0)
		reservesUNI.append(0)
		pairNotExistInUni.append(count1)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6':
			if token1 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesUNI = reserves[0]
				mviReservesUNI = reserves[1]
			reservesUNI.append(Decimal(reserves[0])/Decimal(1000000000000000000))
			reservesUNI.append(Decimal(reserves[1])/Decimal(10**decimals[count1]))
		else:
			if token0 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesUNI = reserves[1]
				mviReservesUNI = reserves[0]
			reservesUNI.append(Decimal(reserves[1])/Decimal(1000000000000000000))
			reservesUNI.append(Decimal(reserves[0])/Decimal(10**decimals[count1]))
	count1 += 1
count2 = 0
reservesSUSHI = []
for i in sushiPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesSUSHI.append(0)
		reservesSUSHI.append(0)
		pairNotExistInSushi.append(count2)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6':
			if token1 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesSUSHI = reserves[0]
				mviReservesSUSHI = reserves[1]
			reservesSUSHI.append(Decimal(reserves[0])/Decimal(1000000000000000000))
			reservesSUSHI.append(Decimal(reserves[1])/Decimal(10**decimals[count2]))
		else:
			if token0 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesSUSHI = reserves[1]
				mviReservesSUSHI = reserves[0]
			reservesSUSHI.append(Decimal(reserves[1])/Decimal(1000000000000000000))
			reservesSUSHI.append(Decimal(reserves[0])/Decimal(10**decimals[count2]))
	count2 += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#The function that help us decide which will should be used to swap token
def whichBeUsedToSwap(x, index):
	x = x*mintData[index]
	temp = reservesUNI[2*index]*reservesSUSHI[2*index+1] - reservesUNI[2*index+1]*reservesSUSHI[2*index] + Decimal(0.997)*x*(reservesUNI[2*index] - reservesSUSHI[2*index])
	if index in pairNotExistInSushi:
		return 1
	elif index in pairNotExistInUni:
		return 2
	elif temp > 0:
		return 1
	else:
		return 2
def calculateBenefit(y, type):
	y = Decimal(y)
	if type == 1:
		benefit = -(reservesUNI[2*quantityOfChildToken]*y*1000)/(997*reservesUNI[2*quantityOfChildToken+1] - 997*y)
		for i in range(quantityOfChildToken):
			if whichBeUsedToSwap(y, i)==2:
				benefit = benefit + (997*mintData[i]*reservesSUSHI[2*i]*y)/(1000*reservesSUSHI[2*i+1]+997*mintData[i]*y)
			else:
				benefit = benefit + (997*mintData[i]*reservesUNI[2*i]*y)/(1000*reservesUNI[2*i+1]+997*mintData[i]*y)
		return benefit
	else:
		benefit = -(reservesSUSHI[2*quantityOfChildToken]*y*1000)/(997*reservesSUSHI[2*quantityOfChildToken+1] - 997*y)
		for i in range(quantityOfChildToken):
			if whichBeUsedToSwap(y, i)==2:
				benefit = benefit + (997*mintData[i]*reservesSUSHI[2*i]*y)/(1000*reservesSUSHI[2*i+1]+997*mintData[i]*y)
			else:
				benefit = benefit + (997*mintData[i]*reservesUNI[2*i]*y)/(1000*reservesUNI[2*i+1]+997*mintData[i]*y)
		return benefit
#This func return an array to determine which used to swap
def getWhichUsedToSwap(y):
	whichUsedToSwap = []
	benefit = -(reservesUNI[2*quantityOfChildToken]*y*1000)/(997*reservesUNI[2*quantityOfChildToken+1] - 997*y)
	for i in range(quantityOfChildToken):
		if whichBeUsedToSwap(y, i)==2:
			whichUsedToSwap.append(2)
			benefit = benefit + (997*mintData[i]*reservesSUSHI[2*i]*y)/(1000*reservesSUSHI[2*i+1]+997*mintData[i]*y)
		else:
			whichUsedToSwap.append(1)
			benefit = benefit + (997*mintData[i]*reservesUNI[2*i]*y)/(1000*reservesUNI[2*i+1]+997*mintData[i]*y)
	return whichUsedToSwap
#This function return the amount of BMVI be minted to reach maximum profit
def calculateMaximumBenefit(left, right, typeSwap):
	while right-left > 0.00000000001:
		x1 = left + (right-left)/3
		x2 = right - (right-left)/3
		if calculateBenefit(x1, typeSwap) > calculateBenefit(x2, typeSwap):
			right=x2
		else:
			left=x1
	return right
#This func return all parametor necessary to the Trader contract	
def getCalculationResult(typeSwap):
	resoult = []
	if typeSwap == 1:
		right = reservesUNI[quantityOfChildToken*2 + 1]
		y = calculateMaximumBenefit(0, right, 1)
		result = Decimal(1000*reservesUNI[quantityOfChildToken*2]*y/(997*(reservesUNI[quantityOfChildToken*2 + 1]-y)))
		temp = int(result*10**18)
		x = Decimal(temp)/Decimal(10**18)
		rIn = Decimal(wethReservesUNI)/Decimal(10**18)
		rOut = Decimal(mviReservesUNI)/Decimal(10**18)
		amoutMviOut =int((997*x*rOut)*10**18/(1000*rIn+997*x))
		whichUsedToSwap = getWhichUsedToSwap(y)
		#>>>>>>>>>>>>>>>>>>>>>>
		money = Decimal(0)
		amountTokenInWallet = []
		#Calculate exactly amout of WETH in wallet before trade
		for i in range(len(data[1])):
			temp_ = int(Decimal(Decimal(data[1][i])*amoutMviOut/1000000000000000000))
			token = Decimal(temp_)/Decimal(10**decimals[i])
			if whichUsedToSwap[i] == 1:
				money = money + (997*reservesUNI[2*i]*token)/(1000*reservesUNI[2*i+1]+997*token)
			else:
				money = money + (997*reservesSUSHI[2*i]*token)/(1000*reservesSUSHI[2*i+1]+997*token)
		amoutOfProfitExpected = int(money*Decimal(10**18))-Decimal(temp)
		allParameter.append([temp, amoutMviOut, amoutOfProfitExpected, data[1], whichUsedToSwap, 1])
		return [temp, amoutMviOut, amoutOfProfitExpected, data[1], whichUsedToSwap,1]
	else:
		right = reservesSUSHI[quantityOfChildToken*2 + 1]
		y = calculateMaximumBenefit(0, right, 2)
		result = Decimal(1000*reservesSUSHI[quantityOfChildToken*2]*y/(997*(reservesSUSHI[quantityOfChildToken*2 + 1]-y)))
		temp = int(result*10**18)
		x = Decimal(temp)/Decimal(10**18)
		rIn = Decimal(wethReservesSUSHI)/Decimal(10**18)
		rOut = Decimal(mviReservesSUSHI)/Decimal(10**18)
		amoutMviOut =int((997*x*rOut)*10**18/(1000*rIn+997*x))
		whichUsedToSwap = getWhichUsedToSwap(y)
		#>>>>>>>>>>>>>>>>>>>>>>
		money = Decimal(0)
		amountTokenInWallet = []
		#Calculate exactly amout of WETH in wallet before trade
		for i in range(len(data[1])):
			temp_ = int(Decimal(Decimal(data[1][i])*amoutMviOut/1000000000000000000))
			token = Decimal(temp_)/Decimal(10**decimals[i])
			if whichUsedToSwap[i] == 1:
				money = money + (997*reservesUNI[2*i]*token)/(1000*reservesUNI[2*i+1]+997*token)
			else:
				money = money + (997*reservesSUSHI[2*i]*token)/(1000*reservesSUSHI[2*i+1]+997*token)
		amoutOfProfitExpected = int(money*Decimal(10**18))-Decimal(temp)
		allParameter.append([temp, amoutMviOut, amoutOfProfitExpected, data[1], whichUsedToSwap,2])
		return [temp, amoutMviOut, amoutOfProfitExpected, data[1], whichUsedToSwap]
#This func create 2 thread calculate parametor when use UNI and SUSHI to swap WETH to BMVI, return the way that make us get maximum benefit
def getAllParameter():
	thread1 = threading.Thread(target=getCalculationResult, args = [1])
	thread2 = threading.Thread(target=getCalculationResult, args = [2])
	thread1.start()
	thread2.start()
	while len(allParameter) < 2:
		continue
	print(allParameter)
	if allParameter[0][2] > allParameter[1][2]:
		return allParameter[0]
	else:
		return allParameter[1] 